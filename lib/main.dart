import 'package:flutter/material.dart';
import 'src/movie_list.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Movies',
        ///Classe MovieList
        home: MovieList(),
    );
  }
}
