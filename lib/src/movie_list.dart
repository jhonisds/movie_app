import 'package:flutter/material.dart';

///import para chamadas para api
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

///Import Class
import 'movie_detail.dart';
import 'movie_item.dart';

class MovieList extends StatefulWidget {
  @override
  _MovieListState createState() => _MovieListState();
}

class _MovieListState extends State<MovieList> {
  /// Armazena dados da API
  var movies;

  ///Get response e update UI
  void getData() async {
    var data = await getJson();
    setState(() {
      ///Update State
      movies = data['results'];
    });
  }

  /// Build UI
  @override
  Widget build(BuildContext context) {
    getData();
    return Scaffold(
      ///bar
      appBar: AppBar(
        title: Text("Movies",
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
        elevation: 0.5,
        centerTitle: false,
        backgroundColor: Colors.blueAccent,

        /// btn menus actions
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.menu),
              onPressed: () => debugPrint("press btn menu")),
          IconButton(
              icon: Icon(Icons.refresh),
              onPressed: () => debugPrint("press btn refresh")),
        ],
      ),

      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          MovieTitle(),
          Expanded(
              child: ListView.builder(
                  itemCount: movies == null ? 0 : movies.length,
                  itemBuilder: (context, i) {
                    return FlatButton(
                        onPressed: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) {
                            return MovieDetails(movies[i]);
                          }));
                        },
                        child: MovieItem(movies, i),
                        color: Colors.white);
                  }))
        ],
      ),
    );
  }
}

/// Widget Título
class MovieTitle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text(
        "Top movies",
        style: TextStyle(
            color: Colors.blueGrey[500],
            fontWeight: FontWeight.bold,
            fontSize: 24.0),
        textAlign: TextAlign.left,
      ),
    );
  }
}

/// Return Future API Json
Future<Map> getJson() async {
  var url =
      "http://api.themoviedb.org/3/discover/movie?api_key=004cbaf19212094e32aa9ef6f6577f22&language=pt-BR";
  http.Response response = await http.get(url);
  print(json.decode(response.body));

  return json.decode(response.body);
}
