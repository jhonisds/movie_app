import 'package:flutter/material.dart';

class MovieItem extends StatelessWidget {
  final movies;
  final item;
  var image_url = 'https://image.tmdb.org/t/p/w500/';

  MovieItem(this.movies, this.item);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                margin: const EdgeInsets.all(16.0),
                child: Container(
                  width: 70.0,
                  height: 70.0,
                ),

                ///Imagem url
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    color: Colors.grey,
                    image: DecorationImage(
                      image:
                          NetworkImage(image_url + movies[item]['poster_path']),
                      fit: BoxFit.cover,
                    ),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.blueGrey[700],
                          blurRadius: 5.0,
                          offset: Offset(2.0, 5.0))
                    ]),
              ),
            ),
            Expanded(
                child: Container(
              margin: const EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 0.0),
              child: Column(
                children: <Widget>[
                  ///Movie Title
                  Text(
                    movies[item]['title'],
                    style: new TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.blueGrey[500]),
                  ),
                  Padding(padding: EdgeInsets.all(2.0)),

                  ///Movie Overview
                  Text(movies[item]['overview'],
                      maxLines: 3,
                      style: new TextStyle(
                        color: Colors.blueGrey[500],
                      )),
                ],
              ),
            ))
          ],
        ),

        ///Divider
        Container(
            width: 300.0,
            height: 0.5,
            color: Colors.grey,
            margin: const EdgeInsets.all(16.0))
      ],
    );
  }
}
