import 'package:flutter/material.dart';
import 'dart:ui' as ui;

class MovieDetails extends StatelessWidget {
  final movie;
  var image_url = 'https://image.tmdb.org/t/p/w500/';

  MovieDetails(this.movie);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      ///[STack] sobrepor Widgets
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Image.network(
            ///[BoxFit.cover] tela inteira
            image_url + movie['poster_path'],
            fit: BoxFit.cover,
          ),

          ///[Blur] Desfoca Imagem / cor do fundo
          BackdropFilter(
            filter: ui.ImageFilter.blur(sigmaX: 5.0, sigmaY: 5.0),
            child: Container(
              color: Colors.black.withOpacity(0.5),
            ),
          ),

          ///scroll view [opaque image]
          SingleChildScrollView(
            child: Container(
              margin: const EdgeInsets.all(20.0),
              child: Column(
                children: <Widget>[
                  ///Container image
                  Container(
                    alignment: Alignment.center,
                    child: Container(width: 400.0, height: 400.0),

                    ///Decorate [Image]
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        image: DecorationImage(
                            image:
                                NetworkImage(image_url + movie['poster_path']),
                            fit: BoxFit.cover),

                        ///BoxShadow [Image]
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black,
                              blurRadius: 20.0,
                              offset: Offset(0.0, 10.0))
                        ]),
                  ),
                  /**Container Text*/
                  Container(
                    margin: const EdgeInsets.symmetric(
                        vertical: 20.0, horizontal: 0.0),

                    ///Horizontal Layout
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            movie['title'],
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 30.0,
                            ),
                          ),
                        ),
                        Text(
                          //Use String Templating
                          '${movie['vote_average']}/10',
                          style: TextStyle(
                            color: Colors.amber,
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold
                          ),
                        ),
                      ],
                    ),
                  ),

                  ///title and vote
                  Text(
                    movie['overview'],
                    style: TextStyle(color: Colors.white),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                  ),

                  ///Buttons
                  Row(
                    children: <Widget>[
                      Expanded(
                          child: Container(
                        width: 150.0,
                        height: 60.0,
                        alignment: Alignment.center,
                        child: Text(
                          'Rate Movie',
                          style: TextStyle(color: Colors.white, fontSize: 20.0),
                        ),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                            color: const Color(0xaa3C3261)),
                      )),
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Container(
                          padding: const EdgeInsets.all(16.0),
                          alignment: Alignment.center,
                          child: Icon(
                            Icons.share,
                            color: Colors.white,
                          ),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              color: const Color(0xaa3C3261)),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          padding: const EdgeInsets.all(16.0),
                          alignment: Alignment.center,
                          child: Icon(
                            Icons.bookmark,
                            color: Colors.white,
                          ),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              color: const Color(0xaa3C3261)),
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
